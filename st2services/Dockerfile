FROM ubuntu:14.04

# Can find package numbers here: https://packagecloud.io/StackStorm/staging-stable
ENV ST2_VERSION="2.0.1-3" \
    ST2_MISTRAL_VERSION="2.0.1-3" \
    ST2_WEB_VERSION="2.0.1-2"

LABEL com.stackstorm.version="${ST2_VERSION}"

RUN apt-get update && \
    apt-get install -y \
      software-properties-common \
      curl && \
    add-apt-repository -y ppa:nginx/stable && \
    curl -s https://packagecloud.io/install/repositories/StackStorm/staging-stable/script.deb.sh | bash && \
    apt-get update && \
    apt-get install -y \
      ca-certificates \
      nginx \
      st2=$ST2_VERSION \
      st2web=$ST2_WEB_VERSION && \
    rm -rf /var/lib/apt/lists/* 


# Install Consul
# Releases at https://releases.hashicorp.com/consul
RUN export CONSUL_VERSION=0.7.0 \
    && export CONSUL_CHECKSUM=b350591af10d7d23514ebaa0565638539900cdb3aaa048f077217c4c46653dd8 \
    && curl --retry 7 --fail -vo /tmp/consul.zip "https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip" \
    && echo "${CONSUL_CHECKSUM}  /tmp/consul.zip" | sha256sum -c \
    && unzip /tmp/consul -d /usr/local/bin \
    && rm /tmp/consul.zip \
    && mkdir /config

# Create empty directories for Consul config and data
RUN mkdir -p /etc/consul \
    && mkdir -p /var/lib/consul


# get ContainerPilot release
ENV CONTAINERPILOT_VERSION 2.4.4
RUN export CP_SHA1=6194ee482dae95844046266dcec2150655ef80e9 \
    && curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CP_SHA1}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /bin \
    && rm /tmp/containerpilot.tar.gz



RUN ln -sf /dev/stdout /var/log/st2/st2api.log && \
    ln -sf /dev/stdout /var/log/st2/st2api.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2auth.log && \
    ln -sf /dev/stdout /var/log/st2/st2auth.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2garbagecollector.log && \
    ln -sf /dev/stdout /var/log/st2/st2garbagecollector.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2notifier.log && \
    ln -sf /dev/stdout /var/log/st2/st2notifier.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2resultstracker.log && \
    ln -sf /dev/stdout /var/log/st2/st2resultstracker.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2rulesengine.log && \
    ln -sf /dev/stdout /var/log/st2/st2rulesengine.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2sensorcontainer.log && \
    ln -sf /dev/stdout /var/log/st2/st2sensorcontainer.audit.log && \
    ln -sf /dev/stdout /var/log/st2/st2stream.log && \
    ln -sf /dev/stdout /var/log/st2/st2stream.audit.log && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log


RUN rm /etc/nginx/sites-enabled/default && \
      mv /etc/nginx/nginx.conf /tmp/nginx.conf && \
      echo "daemon off;" > /etc/nginx/nginx.conf && \
      cat /tmp/nginx.conf >> /etc/nginx/nginx.conf && \
      rm /tmp/nginx.conf


COPY conf/containerpilot.json /tmp/containerpilot.json
COPY conf/htpasswd /etc/st2/htpasswd
COPY conf/nginx.conf /etc/nginx/sites-enabled/st2web.conf
COPY conf/config.js.template /tmp/config.js.template
COPY conf/st2.conf.template /tmp/st2.conf.template
COPY conf/mistral.conf.template /tmp/mistral.conf.template
COPY bin/docker-entrypoint.sh /entrypoint.sh


ENTRYPOINT [ "/entrypoint.sh" ]
