#!/bin/bash

#Consul Var Mappings
consul="${ST2_CONSUL_HOST:-consul}"
nginx="${ST2_NGINX_HOST:-nginx}"

#Rabbit Var Mappings
rmq_host="${ST2_RMQ_HOST:-rmq}"
rmq_port="${ST2_RMQ_PORT:-5672}"
rmq_user="${ST2_RMQ_USER:-rmq-user}"
rmq_pass="${ST2_RMQ_PASS:-rmq-pass}"

#PostGreSQL Var Mappings
pg_username="${ST2_PG_USERNAME:-mistral-user}"
pg_password="${ST2_PG_PASSWORD:-mistral-pass}"
pg_host="${ST2_PG_HOST:-mistral-pg}"
pg_port="${ST2_PG_PORT:-5432}"
pg_name="${ST2_PG_NAME:-mistral}"

#MongoDB Var Mappings
db_host="${ST2_DB_HOST:-st2-mongo}"
db_port="${ST2_DB_PORT:-27017}"
db_name="${ST2_DB_NAME:-st2}"
db_username="${ST2_DB_USERNAME:-st2-user}"
db_password="${ST2_DB_PASSWORD:-st2-pass}"

#Global URL Var Mappings
api_url="${ST2_API_URL:-http://api:9101}"
auth_url="${ST2_AUTH_URL:-http://auth:9100}"
stream_url="${ST2_STREAM_URL:-http://stream:9102}"
mistral_url="${ST2_MISTRAL_URL:-http://mistral/mistral/v2}"
origin_url="${ST2_ORIGIN_URL:-*}"
postgres_url="postgresql://$pg_username:$pg_password@$pg_host:$pg_port/$pg_name"
amqp_url="amqp://$rmq_user:$rmq_pass@$rmq_host:$rmq_port/"


# Generate config file
generate_config_files() {
#/etc/st2/st2.conf  (ST2Services)
  cat /tmp/st2.conf.template | \
    sed -e "s|\$\$api_url|$api_url|" \
 	-e "s|\$\$origin_url|$origin_url|" \
	-e "s|\$\$amqp_url|$amqp_url|" \
	-e "s|\$\$mistral_url|$mistral_url|" \
        -e "s|\$\$db_host|$db_host|" \
        -e "s|\$\$db_port|$db_port|" \
        -e "s|\$\$db_name|$db_name|" \
        -e "s|\$\$db_username|$db_username|" \
        -e "s|\$\$db_password|$db_password|" > /etc/st2/st2.conf

#/opt/stackstorm/static/webui/config.js  (ST2WEB)
  cat /tmp/config.js.template | \
    sed -e "s|\$\$api_url|$api_url|" \
        -e "s|\$\$auth_url|$auth_url|" > /opt/stackstorm/static/webui/config.js

  cat /tmp/containerpilot.json.template | \
    sed -e "s|\$\$consul|$consul|" \

}

generate_config_files

if [ -z $ST2_SERVICE ]; then
  echo "SET THE ST2_SERVICE ENVIRONMENT VARIABLE"
  exit 1
fi

if [ "$ST2_SERVICE" = "st2web" ]; then
  /usr/sbin/nginx
else  
  /opt/stackstorm/st2/bin/$ST2_SERVICE --config-file /etc/st2/st2.conf $@
fi
