FROM postgres:9.6

RUN apt-get update && apt-get install -y \
    bash \
    curl \
    jq \
    unzip && \
    rm -rf /var/lib/apt/lists/*  

# get ContainerPilot release
ENV CONTAINERPILOT_VERSION 2.4.4
ENV CONTAINERPILOT file:///etc/containerpilot.json
RUN export CP_SHA1=6194ee482dae95844046266dcec2150655ef80e9 \
    && curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CP_SHA1}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /bin \
    && mkdir /etc/containerpilot \
    && rm /tmp/containerpilot.tar.gz

#Copy Over Configuration
COPY etc/* /etc/
COPY bin/* /usr/local/bin/


EXPOSE 5432

# override the parent entrypoint
ENTRYPOINT []

CMD [ \
	"containerpilot", \
	"/docker-entrypoint.sh", \
 	"postgres" \	
]
