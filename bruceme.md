

st2api:
  ingress:
    - WebUI
    - CLI
    - ChatOps
  lb:
    - active:active
    - short-lived connections
  port:
    - 9101
   egress:
     - mongodb
     - rabbitmq
  function:
     - REST API for front ends

st2auth:
  ingress:
    - API
    - WebUI
  lb:
    - acitve:active
  port:
    - 9100
  egress:
    - mongodb
    - authenication backend
  function:
    - Provides All Authenication 
  
st2stream:
  ingress:
  lb:
    - active:active
    - long-lived connections
  port:
    - 9102
  egress:
    - mongodb
    - rabbitmq
  function:
    - Exposes a server-sent even stream.

st2sensorcontainer:
  ingress: 
    - not sure
  lb:
    - must be shared / partitioned
    - does not distribute work
  port:
  egress:
  function:

st2rulesengine:
  ingress:
  lb:
    - active:active
  port:
  egress:

st2actionrunner:
  ingress:
  lb:
    - active:active
  port:
  egress:

st2resultstracker:
  ingress:
  lb:
    - Possibility of Extra work
  port:
  egress:
    - mongodb
    - rabbitmq
  function: 
    - Tracks Results of execution handed over to mistral

st2notifier:
  ingress:
  lb:
    - active:active
    - ZooKeeper or Redis required
  port:
  egress:
    - mongodb
    - rabbitmq
  function:
    - Generate st2.core.actiontrigger and st2.core.notifytrigger based on the completion of Action Execution.
    - Act as a backup scheduler for actions that may not have been scheduled. 


st2garbagecollector:
  ingress:
  lb:
    - no perf gained
  port:
  egress:
  function:
    - Cleans up old executions and other operations data based on setup configurations. 
    - By default this does nothing.

mistral-api:
  ingress:
  lb:
  port:
    - 8989
  egress:

mistral-server:
  ingress:
  lb:
  port:
  egress:

mongodb:
  ingress:
  lb:
  port:
  egress:

postgres:
  ingress:
  lb:
  port:
  egress:

rabbitmq:
  ingress:
  lb:
  port:
  egress:











https://packagecloud.io/install/repositories/StackStorm/staging-stable/script.deb.sh 

https://github.com/athlinks/docker-stackstorm/blob/master/stackstorm-1.4/Dockerfile



AMQP_URL - url of rabbitmq, default is: amqp://guest:guest@rabbitmq:5672/.
DB_HOST - mongo database hostname or ip address, default is: mongo.
DB_PORT - mongo database listen port, default is: 27017.
API_URL - stackstorm api endpoint.
 
nginx = 80, 443 tcp

hubot = 8081 tcp

st2auth = 9100 tcp


st2api = 9101 tcp

st2stream = 9102 tcp



  labels:
      - triton.cns.services=st2auth

  labels:
      - triton.cns.services=st2api

  labels:
      - triton.cns.services=st2stream





mongo
  Server:
  needs inital DB creation / Credential  Env vars (dbname=st2, un=none)  
  Explore Replica sets.
  /etc/mongod.conf listen 
  
  Client:
  [database]
    host = <MongoDB host>
    port = <MongoDB server port>
    db_name = <User define database name, usually st2>
    username = <username for db login>
    password = <password for db login>



$ docker exec -it some-mongo mongo admin
connecting to: admin
db.createUser({user: 'st2', pwd: 'st2', roles: [ { role: "dbOwner", db: "st2" } ] });
Successfully added user: {
    "user" : "jsmith",
    "roles" : [
        {
            "role" : "userAdminAnyDatabase",
            "db" : "admin"
        }
    ]
}



mistral
  needs postgres updated /etc/mistral/mistral.conf 
    database.connection : connection = postgresql://<user>:<password>@<database-host>:5432/mistral	
      /opt/stackstorm/mistral/bin/mistral-db-manage --config-file /etc/mistral/mistral.conf upgrade head
      /opt/stackstorm/mistral/bin/mistral-db-manage --config-file /etc/mistral/mistral.conf populate
    default.transport_url
    Edit /etc/st2/st2.conf
    [mistral]
      v2_base_url = https://st2-multi-node-controller/mistral/v2
      api_url = https://st2-multi-node-controller/api


PostGres 

/etc/postgresql/9.3/main/pg_hba.conf
/etc/postgresql/9.3/main/postgresql.conf limit subnet host all all subnet/24 trust


  


