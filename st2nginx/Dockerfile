# a minimal Nginx container including ContainerPilot and a simple virtulhost config
FROM gliderlabs/alpine:3.4

# install nginx and tooling we need
RUN apk update && apk add \
    bash \
    nginx \
    curl \
    unzip && \
    rm -rf /var/cache/apk/*

# we use consul-template to re-write our Nginx virtualhost config
RUN curl -Lo /tmp/consul_template_0.16.0_linux_amd64.zip https://releases.hashicorp.com/consul-template/0.16.0/consul-template_0.16.0_linux_amd64.zip && \
    unzip /tmp/consul_template_0.16.0_linux_amd64.zip && \
    mv consul-template /bin

# get ContainerPilot release
ENV CONTAINERPILOT_VERSION 2.4.4
RUN export CP_SHA1=6194ee482dae95844046266dcec2150655ef80e9 \
    && curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CP_SHA1}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /bin \
    && rm /tmp/containerpilot.tar.gz

# Copy over Entrypoint (Which SED $$consul)
COPY bin/docker-entrypoint.sh /entrypoint.sh

# add ContainerPilot configuration and onChange handler
COPY conf/containerpilot.json.template /tmp/containerpilot.json.template
COPY bin/reload-nginx.sh.template /tmp/reload-nginx.sh.template

# add Nginx virtualhost configuration
COPY conf/nginx.conf /etc/nginx/nginx.conf

# add Nginx virtualhost template that we'll overwrite
COPY conf/nginx.conf.ctmpl /etc/containerpilot/nginx.conf.ctmpl

EXPOSE 80 443

ENTRYPOINT [ "/entrypoint.sh" ]
