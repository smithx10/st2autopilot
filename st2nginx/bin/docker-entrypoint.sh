#!/bin/bash

#Consul Var Mappings
consul="${ST2_CONSUL_HOST:-consul}"

# Generate config file
generate_config_files() {
#container pilot $$consul sed
  cat /tmp/containerpilot.json.template | \
    sed -e "s|\$\$consul|$consul|" > /etc/containerpilot/containerpilot.json

#reload-nginx.ssh $$consul sed
  cat /tmp/reload-nginx.sh.template | \
    sed -e "s|\$\$consul|$consul|" > /bin/reload-nginx.sh
  chmod +x /bin/reload-nginx.sh
}

generate_config_files

/bin/containerpilot -config file:///etc/containerpilot/containerpilot.json nginx -g "daemon off;"
