#!/bin/bash

#Rabbit Var Mappings
rmq_host="${ST2_RMQ_HOST:-rmq}"
rmq_port="${ST2_RMQ_PORT:-5672}"
rmq_user="${ST2_RMQ_USER:-rmq-user}"
rmq_pass="${ST2_RMQ_PASS:-rmq-pass}"

#PostGreSQL Var Mappings
pg_username="${ST2_PG_USERNAME:-mistral-user}"
pg_password="${ST2_PG_PASSWORD:-mistral-pass}"
pg_host="${ST2_PG_HOST:-mistral-pg}"
pg_port="${ST2_PG_PORT:-5432}"
pg_name="${ST2_PG_NAME:-mistral}"

#MongoDB Var Mappings
db_host="${ST2_DB_HOST:-st2-mongo}"
db_port="${ST2_DB_PORT:-27017}"
db_name="${ST2_DB_NAME:-st2}"
db_username="${ST2_DB_USERNAME:-st2-user}"
db_password="${ST2_DB_PASSWORD:-st2-pass}"

#Global URL Var Mappings
api_url="${ST2_API_URL:-http://api:9101}"
auth_url="${ST2_AUTH_URL:-http://auth:9100}"
stream_url="${ST2_STREAM_URL:-http://stream:9102}"
mistral_url="${ST2_MISTRAL_URL:-http://mistral/mistral/v2}"
origin_url="${ST2_ORIGIN_URL:-*}"
postgres_url="postgresql://$pg_username:$pg_password@$pg_host:$pg_port/$pg_name"
amqp_url="rabbit://$rmq_user:$rmq_pass@$rmq_host:$rmq_port"


# Generate config file
generate_config_files() {

#/etc/
  cat /tmp/mistral.conf.template | \
    sed -e "s|\$\$amqp_url|$amqp_url|" \
        -e "s|\$\$postgres_url|$postgres_url|" > /etc/mistral/mistral.conf
}

#Execute generate_config_files fucntion
generate_config_files


#Execute mistral DB
/opt/stackstorm/mistral/bin/mistral-db-manage --config-file /etc/mistral/mistral.conf upgrade head && \
/opt/stackstorm/mistral/bin/mistral-db-manage --config-file /etc/mistral/mistral.conf populate && \
/opt/stackstorm/mistral/bin/mistral-server --server engine,executor --config-file /etc/mistral/mistral.conf --log-file /var/log/mistral/mistral-server.log


